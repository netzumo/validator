package validator

import (
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
	"unicode"

	"github.com/fatih/camelcase"
	"github.com/gin-gonic/gin/binding"
	"gopkg.in/bluesuncorp/validator.v5"
)

const (
	valueSeparator = ";"
	birthdayLayout = "01/02/2006" // MM/DD/YYYY
)

// ValidationErrors validation error representation
type ValidationErrors map[string]string

// Error implement error interface
func (v ValidationErrors) Error() string {
	return "Some field have validation error"
}

// Validator object
type Validator struct {
	SnakeCase bool
	once      sync.Once
	validate  *validator.Validate
}

var _ binding.StructValidator = &Validator{}

// ValidateStruct validate struct
func (v *Validator) ValidateStruct(obj interface{}) error {
	if kindOfData(obj) == reflect.Struct {
		v.lazyInit()
		if err := v.validate.Struct(obj); err != nil {
			return generateValidationErrors(err, v.SnakeCase)
		}
	}
	return nil
}

func (v *Validator) lazyInit() {
	v.once.Do(func() {
		v.validate = validator.New("nzvalid", validator.BakedInValidators)
		v.validate.AddFunction("loginname", loginName)
		v.validate.AddFunction("only", only)
		v.validate.AddFunction("onlyslice", onlySlice)
		v.validate.AddFunction("birthday", birthday)
	})
}

func kindOfData(data interface{}) reflect.Kind {
	value := reflect.ValueOf(data)
	valueType := value.Kind()
	if valueType == reflect.Ptr {
		valueType = value.Elem().Kind()
	}
	return valueType
}

func generateValidationErrors(errStruct *validator.StructErrors, snakeCase bool) ValidationErrors {
	valErr := make(ValidationErrors)
	for field, err := range errStruct.Errors {
		var errMsg string
		switch err.Tag {
		case "alpha":
			errMsg = "must only contain alpha characters"
		case "alphanum":
			errMsg = "must only contain alpha-numeric characters"
		case "email":
			errMsg = "must be a valid email"
		case "len":
			switch err.Kind {
			case reflect.String:
				errMsg = fmt.Sprintf(`must be %s characters long`, err.Param)
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				errMsg = fmt.Sprintf(`must be %s`, err.Param)
			default:
				errMsg = fmt.Sprintf(`must contain %s items`, err.Param)
			}
		case "loginname":
			errMsg = "must only contain letters, numbers, and use '_' or '.' as word separator"
		case "max":
			switch err.Kind {
			case reflect.String:
				errMsg = fmt.Sprintf(`must be less than or equal to %s characters long`, err.Param)
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				errMsg = fmt.Sprintf(`must be less than or equal to %s`, err.Param)
			default:
				errMsg = fmt.Sprintf(`must contain less than or equal to %s items`, err.Param)
			}

		case "min":
			switch err.Kind {
			case reflect.String:
				errMsg = fmt.Sprintf(`must be at least %s characters long`, err.Param)
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				errMsg = fmt.Sprintf(`must be larger than or equal to %s`, err.Param)
			default:
				errMsg = fmt.Sprintf(`must contain at least %s items`, err.Param)
			}
		case "numeric":
			errMsg = "must only contain numeric characters"
		case "required":
			errMsg = "required"
		case "uri":
			errMsg = "must be a valid uri"
		case "url":
			errMsg = "must be a valid url"
		case "only":
			param := strings.Replace(err.Param, ";", ", ", -1)
			errMsg = fmt.Sprintf(`must be one of "%s"`, param)
		case "onlyslice":
			param := strings.Replace(err.Param, ";", ", ", -1)
			errMsg = fmt.Sprintf(`must be an array of "%s"`, param)
		case "birthday":
			errMsg = `must be in the form "MM/DD/YYYY" (ex. 01/20/1999)`
		default:
			errMsg = "Undefined error"
		}
		valErr[cleanUpfield(field, snakeCase)] = errMsg
	}
	return valErr
}

func loginName(top interface{}, current interface{}, field interface{}, params string) bool {
	return _loginName(field.(string))
}

var loginNameRegex = regexp.MustCompile("^[A-Za-z][A-Za-z0-9]*(?:[_.][A-Za-z0-9]+)*$")

func _loginName(s string) bool {
	return loginNameRegex.MatchString(s)
}

func validPassword(top interface{}, current interface{}, field interface{}, params string) bool {
	return _validPassword(field.(string))
}

func _validPassword(s string) bool {
next:
	for _, classes := range [][]*unicode.RangeTable{
		{unicode.Upper, unicode.Title},
		{unicode.Lower},
		{unicode.Number, unicode.Digit},
		{unicode.Space, unicode.Symbol, unicode.Punct, unicode.Mark},
	} {
		for _, r := range s {
			if unicode.IsOneOf(classes, r) {
				continue next
			}
		}
		return false
	}
	return true
}

func only(top interface{}, current interface{}, field interface{}, params string) bool {
	list := strings.Split(params, valueSeparator)

	st := reflect.ValueOf(field)
	switch st.Kind() {
	case reflect.String:
		if st.String() == "" {
			return true
		}
		return inStrSlice(list, st.String())
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		if st.Int() == 0 {
			return true
		}
		val := strconv.FormatInt(st.Int(), 10)
		return inStrSlice(list, val)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		if st.Uint() == 0 {
			return true
		}
		val := strconv.FormatUint(st.Uint(), 10)
		return inStrSlice(list, val)
	}
	panic(fmt.Sprintf(`Bad field type %T in "only" validation`, field))
}

func onlySlice(top interface{}, current interface{}, field interface{}, params string) bool {
	list := strings.Split(params, valueSeparator)

	st := reflect.ValueOf(field)
	switch st.Kind() {
	case reflect.Slice:
		switch st.Type().Elem().Kind() {
		case reflect.String:
			sls := field.([]string)
			for _, e := range sls {
				if !inStrSlice(list, e) {
					return false
				}
			}
			return true
		case reflect.Int:
			sls := field.([]int)
			for _, e := range sls {
				val := strconv.FormatInt(int64(e), 10)
				if !inStrSlice(list, val) {
					return false
				}
			}
			return true
		case reflect.Int64:
			sls := field.([]int64)
			for _, e := range sls {
				val := strconv.FormatInt(e, 10)
				if !inStrSlice(list, val) {
					return false
				}
			}
			return true
		case reflect.Uint:
			sls := field.([]uint)
			for _, e := range sls {
				val := strconv.FormatUint(uint64(e), 10)
				if !inStrSlice(list, val) {
					return false
				}
			}
			return true
		case reflect.Uint64:
			sls := field.([]uint64)
			for _, e := range sls {
				val := strconv.FormatUint(e, 10)
				if !inStrSlice(list, val) {
					return false
				}
			}
			return true
		}

	}

	panic(fmt.Sprintf(`Bad field type %T in "only" validation`, field))
}

func birthday(top interface{}, current interface{}, field interface{}, params string) bool {
	st := reflect.ValueOf(field)
	if st.Kind() == reflect.String {
		if st.String() == "" {
			return true
		}

		if _, err := time.Parse(birthdayLayout, st.String()); err != nil {
			return false
		}
		return true
	}

	panic(fmt.Sprintf(`Bad field type %T in "birthday" validation`, field))
}

func cleanUpfield(s string, snakeCase bool) string {
	fl := camelcase.Split(s)

	if snakeCase {
		return strings.ToLower(strings.Join(fl, "_"))
	}

	if len(fl) <= 1 {
		return strings.ToLower(s)
	}

	ret := make([]string, len(fl))
	ret[0] = strings.ToLower(fl[0])

	for i, v := range fl[1:] {
		a := []rune(strings.ToLower(v))
		a[0] = unicode.ToUpper(a[0])
		ret[i+1] = string(a)
	}
	return strings.Join(ret, "")
}

// asInt returns the parameter as a int64
func asInt(param string) int64 {
	i, err := strconv.ParseInt(param, 0, 64)
	panicIf(err)

	return i
}

// asUint returns the parameter as a uint64
func asUint(param string) uint64 {
	i, err := strconv.ParseUint(param, 0, 64)
	panicIf(err)

	return i
}

// inStrSlice check if value in slice of string
func inStrSlice(sls []string, value string) bool {
	for _, v := range sls {
		if v == value {
			return true
		}
	}
	return false
}

func panicIf(err error) {
	if err != nil {
		panic(err.Error())
	}
}
